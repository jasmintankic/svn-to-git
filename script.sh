set -e
 
#PRE-REQUSITS -> make sure that everything is installed on machine where script is running
# apt-get update
# apt-get install sudo
# sudo apt-get install git-core -y
# sudo apt-get install subversion -y
# sudo apt-get install git-svn -y
# git config --global user.email "jasmin.tankic@bawagpsk.com"
# git config --global user.name "Jasmin Tankic"
 
# YOUR SVN USERNAME
 
SVN_USERNAME="ENTER YOUR USERNAME HERE"
 
echo "**************************************************"
echo "MIGRATION FROM SVN TO GIT STARTED BY USER: p015787"
echo "**************************************************"
 
# PROJECT DIRECTORY
echo "PLEASE ENTER SVN REPO DIRECTORY:"
read SVN_REPO_DIRECTORY
 
echo "PLEASE ENTER GITHUB REPO URL:"
read GIT_REPO_URL
 
# PROJECT FOLDER NAME -> in this folder project will be cloned
echo "PLEASE ENTER PROJECT FOLDER NAME:"
read PROJECT_NAME
 
# echo "****** PROCESSING ALL AUTHORS FROM THE SVN REPO ******"
# SVN_REPO_FOLDER=svn_repo_$SVN_USERNAME
 
# rm -r $PROJECT_NAME
# rm -r $SVN_REPO_FOLDER
 
# echo "****** CREATING SVN REPO TO FETCH ALL USERS ******"
# mkdir $SVN_REPO_FOLDER
# ls
 
# cd $SVN_REPO_FOLDER
# echo "****** CHEKING OUT TO REQUESTED SVN PROJECT ******"
# svn co --username $SVN_USERNAME --password Bawag2112! $SVN_REPO_DIRECTORY/trunk
# echo "****** CHECK OUT COMPLETED ******"
 
# cd trunk
# echo "****** GETTING ALL AUTHORS AND GENERATING authors.txt file ******"
# svn log -q | awk -F '|' '/^r/ {sub("^ ", "", $2); sub(" $", "", $2); print $2" = "$2" <"$2">"}' | sort -u > authors.txt
# echo "****** AUTHORS PROCESSING COMPLETED ******"
# cd ..
# cd ..
 
echo "****** STARTING MIGRATION TO GITHUB: ******"
echo "****** IF MIGRATION FAILS PLEASE RUN 'git svn fetch' TO CONTINUE WITH MIGRATION ******"
 
# LONG COMMAND TO DO MIGRATION
#git svn clone --authors-file=$SVN_REPO_FOLDER/trunk/authors.txt --username=p015787 --no-metadata $SVN_HOST --trunk $SVN_REPO_DIRECTORY/trunk --branches $SVN_REPO_DIRECTORY/branches --tags $SVN_REPO_DIRECTORY/tags $PROJECT_NAME
 
# SHORT COMMAND TO DO MIGRATION
# git svn clone $SVN_REPO_DIRECTORY --authors-file=$SVN_REPO_FOLDER/trunk/authors.txt --no-metadata --username $SVN_USERNAME -s $PROJECT_NAME
 
 
mkdir $PROJECT_NAME
cd $PROJECT_NAME
git svn init $SVN_REPO_DIRECTORY -T trunk -b branches -t tags
git svn fetch --log-window-size=80000
 
echo "****** MIGRATION FROM SVN TO GIT SUCCESSFULLY COMPLETED ******"
 
echo "****** PROCESSING TAGS AND BRANCHES ******"
 
git for-each-ref --format="%(refname:short) %(objectname)" refs/remotes/origin/tags \
| while read BRANCH REF
  do
        TAG_NAME=${BRANCH#*/}
        BODY="$(git log -1 --format=format:%B $REF)"
 
        echo "ref=$REF parent=$(git rev-parse $REF^) tagname=$TAG_NAME body=$BODY" >&2
 
        git tag -a -m "$BODY" $TAG_NAME $REF^  &&\
        git branch -r -d $BRANCH
  done
 
echo "***** PROCESSING TAGS AND BRANCHES SUCESSFULLY COMPLETED ******"
 
echo "****** DELETING TAG BRANCHES ******"
 
git for-each-ref --format="%(refname:short)" refs/remotes/origin/tags | cut -d / -f 2- |
while read ref
do 
  echo git branch -rd $ref
done
 
echo "****** FINISHED ******* DELETING TAG BRANCHES ******"
 
echo "****** CORRECT TAG NAMES ******"
 
git for-each-ref --format="%(refname:short)" refs/remotes/origin/tags |
while read ref
do
  tag=`echo $ref | sed 's/_/./g'` # give tags a new name
  echo $ref -\> $tag
  git tag -a $tag `git rev-list -2 $ref | tail -1` -m "proper svn tag"
done
 
echo "****** FINISHED ******* CORRECT TAG NAMES ******"
 
echo "****** CREATING BRANCHES ******"
 
git for-each-ref --format="%(refname:short) %(objectname)" refs/remotes/origin \
| while read BRANCH REF
  do
        TAG_NAME=${BRANCH#*/}
 
        git branch $TAG_NAME remotes/$BRANCH
  done
 
echo "****** FINISHED ****** CREATING BRANCHES ******"
 
echo "****** Removing peg-revisions ******"
 
for p in $(git for-each-ref --format='%(refname:short)' | grep @); do git branch -D $p || continue; done
 
echo "****** REMOVING SVN TRUNK BRANCH ******"
git branch -d trunk
 
echo "****** ADDING PROJECT TO GIT ******"
 
git remote add origin $GIT_REPO_URL
 
 
 
############################################################
 
 
#!/bin/bash
 
################################################################################
########################## chunked-push.sh #####################################
################################################################################
 
# LEGEND:
# Repositories larger than 2GB cannot be pushed to github.com
#
# This script attempts to push one branch from such a repository in
# chunks smaller than 2GB. Make sure to use SSH as push protocol as
# HTTPS frequently runs into trouble with these large pushes.
#
# Run this script within the repository and pass the name of the
# remote as first argument.
#
# The script creates some temporary local and remote references with names like
#
#     refs/github-sevices/chunked-upload/*
#
# If it completes successfully, it will clean up those references.
#
# Example: chunked-push.sh origin
#
########################
# Set to exit on error #
########################
set -e
 
# DRY_RUN can be set (either by uncommenting the following line or by
# setting it via the environment) to test this script without doing
# any any actual pushes.
# DRY_RUN=1
 
# MAX_PUSH_SIZE is the maximum estimated size of a push that will be
# attempted. Note that this is only an estimate, so it should probably
# be set smaller than any hard limits. However, even if it is too big,
# the script should succeed (though that makes it more likely that
# pushes will fail and have to be retried).
 
########
# VARS #
########
# Default max push size is 1GB
MAX_PUSH_SIZE="${MAX_PUSH_SIZE:-1000000000}"
# Remote to push towards, default origin
REMOTE="${1:-origin}"
# Prefix for temorary refs created
REF_PREFIX='refs/github-services/chunked-upload'
# Tip commit of the current branch that we want to push to GitHub
HEAD="$(git rev-parse --verify HEAD)"
# Name of the current branch that we want to push to GitHub
BRANCH="$(git symbolic-ref --short HEAD)"
# Options to push
PUSH_OPTS="--no-follow-tags"
 
################################################################################
########################## FUNCTIONS BELOW #####################################
################################################################################
################################################################################
#### Function Header ###########################################################
Header() {
  echo ""
  echo "-------------------------------"
  echo "-- Push repository in chunks --"
  echo "-------------------------------"
  echo ""
  echo "Gathering information from local repository..."
}
################################################################################
#### Function Git_Push #########################################################
Git_Push() {
  if test -n "${DRY_RUN}"; then
    # Just show what push command would be run, without actually
    # running it:
    echo git push "$@"
  else
    git push "$@"
  fi
}
################################################################################
#### Function Estimate_Size ####################################################
Estimate_Size() {
  # usage: Estimate_Size [REV]
  #
  # Return the estimated total on-disk size of all unpushed objects that
  # are reachable from REV (or ${HEAD}, if REV is not specified).
  local REV=''
  REV="${1:-$HEAD}"
 
  git for-each-ref --format='^%(objectname)' "${REF_PREFIX}" |
    git rev-list --objects "${REV}" --stdin |
    awk '{print $1}' |
    git cat-file --batch-check='%(objectsize:disk)' |
    awk 'BEGIN {sum = 0} {sum += $1} END {print sum}'
}
################################################################################
#### Function Check_Size #######################################################
Check_Size() {
  # usage: Check_Size [REV]
  #
  # Check whether a push of REV (or ${HEAD}, if REV is not specified) is
  # estimated to be within $MAX_PUSH_SIZE.
  local REV=''
  REV="${1:-$HEAD}"
  local SIZE=''
  SIZE="$(Estimate_Size "${REV}")"
 
  if test "${SIZE}" -gt "${MAX_PUSH_SIZE}"; then
    echo >&2 "size of push is predicted to be too large: ${SIZE} bytes"
    return 1
  else
    echo >&2 "predicted push size: ${SIZE} bytes"
  fi
}
################################################################################
#### Function Push_Branch ######################################################
Push_Branch() {
  # usage: Push_Branch
  #
  # Check whether a push of ${BRANCH} to ${REMOTE} is likely to be within
  # $MAX_PUSH_SIZE. If so, try to push it. If not, emit an informational
  # message and return an error.
  Check_Size &&
  Git_Push ${PUSH_OPTS} --force "${REMOTE}" "${HEAD}:refs/heads/${BRANCH}"
}
################################################################################
#### Function Push_Rev #########################################################
Push_Rev() {
  # usage: Push_Branch REV
  #
  # Check whether a push of REV to ${REMOTE} is likely to be within
  # $MAX_PUSH_SIZE. If so, try to push it to a temporary reference. If
  # not, emit an informational message and return an error.
  local REV="$1"
 
  Check_Size "${REV}" &&
  Git_Push ${PUSH_OPTS} --force "${REMOTE}" "${REV}:${REF_PREFIX}/${REV}"
}
################################################################################
#### Function Push_Chunk #######################################################
Push_Chunk() {
  # usage: Push_Chunk
  #
  # Try to push a portion of the contents of ${HEAD}, such that the amount
  # to be pushed is estimated to be less than $MAX_PUSH_SIZE. This is
  # done using the same algorithm as 'git bisect'; namely, by
  # successively halving of the number of commits until the size of the
  # commits to be pushed is less than $MAX_PUSH_SIZE. For simplicity and
  # to avoid extra estimation work, instead of trying to find the
  # optimum number of commits to push, we stop as soon as we find a
  # range that meets the criterion. This will typically result in a push
  # with a size approximately in the range
  #
  #     $MAX_PUSH_SIZE / 2 <= size <= $MAX_PUSH_SIZE
  CHUNK_SIZE="${HEAD}"
  LAST_REV=''
 
  while true; do
    # find a new midpoint, this call sets ${bisect_rev} and $bisect_steps
    # Note: $bisect_rev and $bisect_steps are ENV vars and need to be lower case
    eval "$(
      git for-each-ref --format='^%(objectname)' "${REF_PREFIX}" |
        git rev-list --bisect-vars "${CHUNK_SIZE}" --stdin
    )"
 
    # Check to see if we have hit the bottom and cant get smaller
    if [ "${bisect_rev}" == "${LAST_REV}" ] && [ -n "${bisect_rev}" ] && [ -n "${LAST_REV}" ]; then
      # ERROR
      echo >&2 "We have hit the smallest commit:[${bisect_rev}] and its larger than allowed upload size!"
      exit 1
    fi
 
    # Try to push the bisect rev
    echo >&2 "attempting to push:[${bisect_rev}]..."
    if Push_Rev "${bisect_rev}"; then
      # Success
      echo >&2 "push succeeded!"
      git update-ref "${REF_PREFIX}/${bisect_rev}" "${bisect_rev}"
      return
    else
      # Failure
      echo >&2 "push failed; trying a smaller chunk"
      # Set the local vars
      CHUNK_SIZE="${bisect_rev}"
      LAST_REV="${bisect_rev}"
    fi
  done
}
################################################################################
############################### MAIN ###########################################
################################################################################
 
##########
# Header #
##########
Header
 
############################
# Start to push the chunks #
############################
while ! Push_Branch ; do
  echo >&2 "trying a partial push"
  Push_Chunk
done
 
###########################################
# Clean up the local temporary references #
###########################################
git for-each-ref --format='delete %(refname)' "${REF_PREFIX}" |
  git update-ref --stdin
 
############################################
# Clean up the remote temporary references #
############################################
Git_Push ${PUSH_OPTS} --prune "${REMOTE}" "${REF_PREFIX}/*:${REF_PREFIX}/*"
 
 
 
 
 
 
 
#########################################
 
git push origin --all
git push origin --tags
 
cd ..
 
rm -r $PROJECT_NAME
# rm -r $SVN_REPO_FOLDER
 
echo "DONE YOUR REPO IS SUCCESSFULLY MIGRATED ON: " $GIT_REPO_URL

